From mathcomp Require Import all_ssreflect.
From mathcomp Require Import all_algebra.
From mathcomp Require Import seq.
From mathcomp Require Import path.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Import Num.Theory.

Open Scope ring_scope.

Section Defs.

Variable R : numDomainType.


Record point: Type := Point {x_coord : R; y_coord : R}.
Notation "( x , y )" := (Point x y).
Notation "p .x" := (x_coord p) (at level 2).
Notation "p .y" := (y_coord p) (at level 2).
Notation "p1 =p p2" := ((p1.x == p2.x) && (p1.y == p2.y)) (at level 1).


Record seg: Type := Seg {sp : point; ep: point}.
Inductive option A := None | Some (a: A).

Notation "[ a , b ]" := (Seg a b).

Open Scope ring_scope.

Locate "+".

Definition slope (s: seg) := let: [a, b] := s in (b.y - a.y)/(b.x - a.x).

Definition prj_Y (s: seg) (p: point) :=
  let: [a, b] := s in
    ((slope s)*(p.x - a.x) + a.y).


Definition prj (s: seg) (p: point) := (p.x, prj_Y s p).

Lemma idempot_prj (s: seg) (p: point): prj s (prj s p) = prj s p.
Proof.
by rewrite /prj /prj_Y /slope.
Qed.


Definition same_R := fun (x : R) (o : option R) =>
  if o is Some y then x == y
  else false.

Definition same_P := fun (p : point) (o : option point) =>
  if o is Some p' then (p.x == p'.x)&&(p.y == p'.y)
  else false.

Check [::0].

Fixpoint purge_P_aux (l : list point) o :=
  match l with
  | nil => nil
  | x::l' =>if same_P x o then purge_P_aux l' o 
             else x::(purge_P_aux l' (Some x))
  end.

Fixpoint purge_P l :=
  match l with
  |nil => nil
  |[::x] => [::x]
  |x::((y::l') as l) => if x =p y then purge_P l else x::(purge_P l)
  end.

Definition check  := fun (s: seg) (p : point) =>
  let: [a, b] := s in (p.x >= a.x) && (p.x <= b.x).

Definition o_max_o := fun (x :R) o =>
  match o with
  |None => Some x
  |Some y => if x >= y then Some x else Some y
  end.

Fixpoint up p l :=
  match l with
  |nil => None seg
  |s::t =>
    if check s p then
      (if prj_Y s p <= p.y then up p t
      else
        (if up p t is Some s' then
          if prj_Y s p <= prj_Y s' p then Some s else Some s'
         else Some s))
    else up p t
  end.

Fixpoint down p l :=
  match l with
  |nil => None (seg)
  |s::t =>
    if check s p then
      (if prj_Y s p >= p.y then down p t
      else
        (if up p t is Some s' then
          if prj_Y s p >= prj_Y s' p then Some s else Some s'
         else Some s))
    else up p t
  end.


Record open_cell: Type := Open_cell {st: point; floor: seg; ceiling: seg}.

Notation "[ s , f , c ]" := (Open_cell s f c).

Definition is_eq_s := fun (s: seg) (s': seg) => (((sp s) =p (sp s'))&&((ep s) =p (ep s'))).

Definition same_S := fun (s: seg) (o: option seg) =>
  match o with
  |Some s' => s = s'
  |None => False
  end.

Inductive double (A B: Type) := mk_double (a : A) (b : B).

Notation "( a ; b )" := (mk_double a b).

Definition fst A B (d: double A B) := let: (a; _) := d in a.
Definition snd A B (d: double A B) := let: (_; b) := d in b.


Definition extend A := fun (f: A -> A -> bool) (x: A) (o: option A) =>
  match o with
    |Some y => f x y
    |None => false
  end.

Fixpoint sep_concerned (p: point) (d: option seg) (l: list open_cell) :=
  if l is c::l' then
  (
    if extend is_eq_s (floor c) d || ((ep (floor c)) =p p) || ((ep (ceiling c)) =p p) then
      let: dbl := (sep_concerned p d l') in
      (fst dbl; c::(snd dbl))
    else
      let: dbl := sep_concerned p d l' in
      (c::(fst dbl); snd dbl)
  )
  else (nil; nil).

Definition segs_aux := fun (u: option seg) (d: option seg) (l: list seg) =>
  (match d with |Some sd => sd::l |None =>l end)++(match u with |Some su => [:: su] |None => nil end).

Fixpoint open_newL (p: point) (l: list seg) :=
  match l with
  |nil => nil
  |[::x] => nil
  |a::((b::l') as tl) => [p, a, b]::(open_newL p tl)
  end.

Definition close_cell := fun (p: point) (c: open_cell) =>
  purge_P [:: st c; prj (floor c) (st c); prj (floor c) p; p; prj (ceiling c) p; prj (ceiling c) (st c); st c].

Definition step := fun (sL : list seg) (cL : list open_cell) (p: point) (psL: list seg) =>
  let: u := up p sL in
  let: d := down p sL in
  let: (rem_cell; cells) := sep_concerned p d cL in
  let: new_cells := open_newL p (segs_aux u d psL) in
  (rem_cell ++ new_cells ; map (close_cell p) cells).

Definition decomp := fun (pcL : list (double point (list seg))) (sL: list seg) =>
  snd (foldl (fun (db : double (list open_cell) (list (list point))) (pc: double point (list seg)) =>
    let: (cL; cellL) := db in
    let: (cL'; c) := step sL cL (fst pc) (snd pc) in
    (cL' ; c++cellL)) (nil ; nil) pcL).

Fixpoint inser_point (p: point) (l: list (double point (list seg))) :=
  match l with
  |nil => [::(p; nil)]
  |(p'; lp)::l' =>
    if p'.x > p.x then (p;nil)::((p; lp)::l')
    else (if p'.x < p.x then (p; lp)::(inser_point p l')
    else (p'; lp)::l')
  end.

Fixpoint inser_seg (s: seg) (l: list (double point (list seg))) :=
  match l with
  |nil => inser_point (ep s) ([:: (sp s; [::s])])
  |(p; l')::tl =>
    let: ps := sp s in 
    if p.x < ps.x then (p; l')::(inser_seg s tl)
    else if p.x > ps.x then (ps; [::s])::(inser_point (ep s) l)
    else (p; l'++[::s])::(inser_point (ep s) tl)
  end.

Definition slope_comp := fun (s: seg) (s': seg) => slope s <= slope s'.

Definition point_eq := fun (p: point) (p': point) => (p.x == p'.x)&&(p.y == p'.y).

Lemma point_eqP : Equality.axiom point_eq.
Proof.
move=> [x1 y1] [x2 y2] /=.
rewrite /point_eq /=.
have [/eqP xeq | xneq] := boolP (x1 == x2).
  have [/eqP yeq | yneq] := boolP (y1 == y2).
    by apply: ReflectT; rewrite xeq yeq.
  by rewrite andbF; apply: ReflectF; move => [] _ /eqP; rewrite (negbTE yneq).
by apply: ReflectF; move=> [] /eqP; rewrite (negbTE xneq).
Qed.

Canonical point_eqType := EqType point (EqMixin point_eqP).

Definition seg_eq := fun (s: seg) (s':seg) => (sp s == sp s')&&(ep s == ep s').

Lemma seg_eqP : Equality.axiom seg_eq.
Proof.
move=> [a1 b1] [a2 b2] /=.
rewrite /seg_eq /=.
have [/eqP aeq | aneq] := boolP (a1 == a2).
  have [/eqP beq | bneq] := boolP (b1 == b2).
    by apply: ReflectT; rewrite aeq beq.
  by rewrite andbF; apply: ReflectF; move => [] _ /eqP; rewrite (negbTE bneq).
by apply: ReflectF; move => [] /eqP; rewrite (negbTE aneq).
Qed.

Canonical seg_eqType := EqType seg (EqMixin seg_eqP).

(*Definition get_points := fun (sL: list seg) =>
  foldr inser_seg nil (sort slope_comp sL).

Definition main sL := fun (sL: list seg)
  decomp get_points sL sL*)

(*Definition big_in A := fun (x: option A) (l: list A) =>
  match x with
    |Some a => a \in l
    |None => false
  end.

Lemma up_in_l p sL : up p sL \in sL.*)

(*Up
Montrer que le segment [p, prj (up p l) p] ne rencontre aucun obstacle dans l.
up p l \in l.
up p l = Some s -> p entre les abscisses des extrémités de s.
*)
End Defs.