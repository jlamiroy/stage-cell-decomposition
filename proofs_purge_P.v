From mathcomp Require Import all_ssreflect.
From mathcomp Require Import all_algebra.
From mathcomp Require Import seq.
From mathcomp Require Import path.

Require Import proof.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Section examples.

Variable R : numDomainType.

Lemma purge_P_step (a b : point R) tl :
  purge_P (a :: b :: tl) =
  if a == b then purge_P (b :: tl) else a :: purge_P (b :: tl).
Proof. by []. Qed.

Lemma purge_P1 (l : seq (point R)) x :  x \in purge_P l <-> x \in l.
Proof.
elim l => [ | a [ | b tl] IH] //.
  rewrite purge_P_step.
have [/eqP -> | anb] := boolP (a == b).
  rewrite !inE orbA.
  rewrite orbb.
  rewrite IH.
  rewrite //. 
  (*by rewrite !inE orbA orbb IH inE.*)
rewrite !inE. split; (move=> /orP [];[move=> -> //| ]).
  rewrite IH.
  move => it.
  apply /orP.
  right.
  by [].

  move=> it.
  apply /orP.
  right.
  rewrite IH.
  by [].
  (*by rewrite IH inE => it; apply/orP; right.*)
(*by move=> it; apply/orP; right; rewrite IH inE.*)
Qed.

Lemma purge_P_base1 (x: point R): purge_P [::x] = [::x].
Proof.
by [].
Qed.


(* I verified that this proof is feasible. *)
Lemma purge_P_head (l : seq (point R)) x : exists l', purge_P (x::l) = x::l'.
Proof.
elim: l x => [| a [| b tl] IH] x//.
  by exists nil.
  rewrite purge_P_step.
  have [/eqP -> | anx] := boolP (x == a).
  by exists nil.
  by exists [::a].
  rewrite purge_P_step.
  have [/eqP -> | anx] := boolP (x == a).
  apply (IH a).
  by exists (purge_P [:: a, b & tl]).
  (*case: (IH a).
  move=> x0.
  by exists x0.*)
  
 (*apply (IH a).*)
Qed.

(* I verified that this proof is feasible. *)
Lemma purge_P_inside (l l' : seq (point R)) : exists l2,
   purge_P (l ++ l') = l2 ++ purge_P l'.
Proof.
  elim l => [| a [|b tl] IH] //.
  by exists nil.
  rewrite cat1s.
  case l' => [|p l0].
  rewrite /=.
  by exists [:: a].
  (*move=> p l0.*)
  rewrite purge_P_step.
  have [/eqP | anp] := boolP (a == p).
  by exists nil.
  by exists [::a].

  rewrite cat_cons cat_cons purge_P_step.
  have [aeb | anb] := boolP (a == b).
  apply IH.
  (*case: IH.
  move => x.
  exists x.
  by rewrite p.*)

  (*apply IH.*)
  case: IH => x.
  exists (a::x).
  by rewrite p.
Qed.

Lemma ex_nihilo (l' l'': seq (point R)): nil = l' ++ l'' -> l' = nil /\ l'' = nil.
Proof.
case l' => [| p l].
by [].
by [].
Qed.

Lemma singel1 (a: point R) (b: point R) (l: seq (point R)): [::a] = b::l -> l = nil.
Proof.
case l => [| p tl].
by [].
by [].
Qed.

Lemma singleton (a: point R) (l' l'': seq (point R)): [::a] = l' ++ l'' -> (l' = nil /\ l'' = [::a]) \/ (l' = [::a] /\ l'' = nil).
Proof.
case l' => [| p [|q l]].
rewrite /=.
by left.
rewrite /=.
move=> it.
have nihil: l'' = nil.
move: it.
apply singel1.
move: it.
rewrite nihil.
by right.
by [].
Qed.

(* I don't have a complete version of this one yet. *)
Lemma purge_P_inside2 (l l' l'' : seq (point R)) :
  purge_P l = l' ++ l'' -> exists l1 l2,  l = l1 ++ l2 /\
   l'' = purge_P l2.
Proof.
  elim: l l' l'' => [| a [|b tl] IH] l' l''.
  rewrite /=.
  move=> Hnil.
  have nihil: l' = nil /\ l'' = nil.
  by apply ex_nihilo.
  exists nil.
  exists nil.
  by case nihil.
  rewrite /=.
  move=> it.
  have sg: (l' = nil /\ l'' = [::a]) \/ (l' = [::a] /\ l'' = nil).
  by apply singleton.
  case sg => [sg1 | sg2].
  exists nil.
  exists [::a].
  case: sg1.
  by [].
  exists [::a].
  exists nil.
  case: sg2.
  by [].
  
  case l' => [| p tl'].
  rewrite cat0s.
  exists nil.
  exists [::a, b &tl].
  by [].

  rewrite purge_P_step.
  have [/eqP -> | anb] := boolP (a == b).
  move=> it.
  case: (IH (p::tl') l'') => [| x [y]].
  by [].
  exists (b::x).
  exists y.
  case p0.
  move=> p1 p2.
  by rewrite p1 p2.

  rewrite cat_cons.
  move => it.
  have hsame: head a (a::purge_P(b::tl)) = head a (p::tl' ++ l'').
  by rewrite it.

  move: hsame.
  rewrite /head.
  move=> aep.
  move: it.
  rewrite aep.
  move=> it.

  have same_behead: behead (p::purge_P(b::tl)) = behead (p::tl'++l'').
  by rewrite it.

  move: same_behead.
  rewrite /behead.
  move=> it2.

  case: (IH tl' l'') => [|x [y]].
  by [].
  move=> Pxy.
  case Pxy.
  move=> Px Py.
  exists (p::x).
  exists y.
  by rewrite Px Py.
Qed.
